#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int primes[] = {2, 3,	5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
  int numb = atof(argv[1]);
  int flag = 0;
// Initialize the MPI environment
  MPI_Init(NULL, NULL);
  // Find out rank, size
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  int div = 0;
  int token;
  // Receive from the lower process and send to the higher process. Take care
  // of the special case when you are the first process to prevent deadlock.
if (world_rank == 0)
{
 token = numb;
 MPI_Send(&token, 1, MPI_INT, (world_rank + 1) % world_size, 0, MPI_COMM_WORLD);
}
while(1 == 1)
{  
    if (world_rank != 0) {
    MPI_Recv(&token, 1, MPI_INT, world_rank - 1, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    printf("Recive %d at %d\n", token, world_rank);
    if(token % primes[world_rank-1] == 0)
    {
      div++;
      token = token / primes [world_rank-1];
    }
    else
    {
       div = div;
    }
    printf("Send %d \n",token);
    //token = token / primes [world_rank-1];
  MPI_Send(&token, 1, MPI_INT, (world_rank + 1) % world_size, 0,
           MPI_COMM_WORLD);
if (token == 1)
{break;}  
}
  // Now process 0 can receive from the last process. This makes sure that at
  // least one MPI_Send is initialized before all MPI_Recvs (again, to prevent
  // deadlock)
  if (world_rank == 0) {
    MPI_Recv(&token, 1, MPI_INT, world_size - 1, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    printf("Process %d received token %d from process %d\n", world_rank, token,
           world_size - 1);
   if(flag == 0)
   {
     flag = token;
   }
   else
   {
     if(flag == token)
     {token = 1;}
     else
     {flag = token;}
   }
MPI_Send(&token, 1, MPI_INT, (world_rank + 1) % world_size, 0,MPI_COMM_WORLD);
if (token == 1)
{break;}
  }
}
int *sub = NULL; 
if (world_rank == 0) 
{   sub = malloc(sizeof(int) * world_size); }
MPI_Gather(&div, 1, MPI_INT, sub, 1, MPI_INT, 0, MPI_COMM_WORLD);
if (world_rank == 0)
{
printf("Flag = %d\n", flag);
int i = 1;
while(i < world_size)
{
printf("test %d\n", sub[i]);
i++;
}
}
  MPI_Finalize();
//printf("test %d\n", 5);
}
